import logging
import os
import threading
import time

from emoji import emojize
from pubsub import pub
from ska_oso_oet.event import topics

LOG = logging.getLogger(__name__)
FORMAT = '%(asctime)-15s %(message)s'

logging.basicConfig(level=logging.INFO, format=FORMAT)


def announce(msg: str):
    """
    Helper function to send OET messages

    :param msg: message to announce
    """
    pub.sendMessage(
        topics.user.script.announce,
        msg_src=threading.current_thread().name,
        # uses emoji library function. Not installed in default environment
        msg=emojize(msg)
    )


def main(msg=None):
    # sleep to give CLI chance to connect to SSE stream
    time.sleep(1)
    announce(f':desktop_computer: running git script in OS process {os.getpid()}')

    num_scans = 5
    for i in range(1, num_scans+1):
        announce(f':telescope: executing scan {i}/{num_scans}')
        time.sleep(1)
        announce(f':telescope: scan {i} complete')
        if msg:
            announce(msg)

    announce(f':desktop_computer: script complete')
